# Laravel Repositories Pattern
### Things todo list
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-repositories-pattern.git`
2. Go inside the folder: `cd laravel-repositories-pattern`
3. Run `cp .env.example .env` then set your desired database name, username & password
4. Run `php artisan migrate --seed`
5. Run `php artisan serve`
4. Open your favorite browser: http://localhost:8000

### Screen shot

List all users

![List all users](img/list.png "List all users")

Find user by User ID

![Find user by User ID](img/id.png "Find user by User ID")

Find user by User Email

![Find user by User Email](img/email.png "Find user by User Email")
